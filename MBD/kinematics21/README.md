# Free Falling Body

+ MultiBody
+ Discret Element
+ Space Time graph

 File                                   | Contents    
 :-------------                         | :-------------
 [particle.mbd](particle.mbd)           | MBDyn Input File
 [Makefile](Makefile)                   | Makefile for running the analysis

## Model Description

First problem of the ADAMS verification guide based on Beer and Johnsnton, Dynamics,
4th edition, 424 page problem.

A particle P is moving along a straight line such taht the position x of P relative
to a fixed point O is defined by the equation $`x = t^3 - 6t^2 -15t + 40`$, where 
$`t`$ denotes the time expressed in seconds. Determine:

- Time at which the magnitude $`v`$ of the velocity of the particle is zero.
- The position $`x`$ and distance $`d`$ traveled at that time.
- The magnitude of the acceleration of the particle at that time.
- The distance $`d`$ traveled from $`t=4`$ seconds to $`t=6`$ seconds.

![](refs/particle.png)


## Results

