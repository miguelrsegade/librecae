# Free Rotating block

+ MultiBody
+ Rigid Body motion
+ Rotation

 File                                         | Contents    
 :-------------                               | :-------------
 [frblock.mbd](frblock.mbd)                   | MBDyn Input File
 [angularVelocity.py](angularVelocity.png)    | Generates the plot of the 3 components of the angular velocity
 [trajectory.py](trajectory.png)              | Generates the trajectory of one of corners of the block
 [Makefile](Makefile)                         | Makefile for running the analysis

## Model Description

Adapted problem using python taken from here: 
<http://www.sky-engin.jp/en/MBDynTutorial/chap08/chap08.html>

![](refs/block.png)

Parameters   | Value
:----------  | :-------------
Lx            | 0.15 m
Ly            | 0.05 m
Lz            | 0.3 m
wx            | 5 rad/s
wy            | 0.02 rad/s
wz            | 0.02 rad/s
M             | 1 kg

## How to run it
```
$ make
```

## Solution obtained

![](refs/angularVelocity.png)
![](refs/trajectory.png)

