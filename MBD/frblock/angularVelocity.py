import numpy as np
import matplotlib.pyplot as plt
import netCDF4

data = netCDF4.Dataset('frblock.nc', 'r')

NodeLabel = 1
omega = data.variables['node.struct.' + str(NodeLabel) + '.Omega']
t = data.variables['time']

plt.plot(t, omega[:,0],  color='b')
plt.plot(t, omega[:,1],  color='g')
plt.plot(t, omega[:,2],  color='r')
plt.savefig('angularVelocity.png')
