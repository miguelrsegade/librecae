import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as plticker
import netCDF4

data = netCDF4.Dataset('frblock.nc', 'r')

NodeLabel = 2
position = data.variables['node.struct.' + str(NodeLabel) + '.X']
t = data.variables['time']

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(position[:,0], position[:,1], position[:,2])

locx = plticker.MultipleLocator(base=0.05)
locy = plticker.MultipleLocator(base=0.1)
locz = plticker.MultipleLocator(base=0.1)
ax.xaxis.set_major_locator(locx)
ax.yaxis.set_major_locator(locy)
ax.zaxis.set_major_locator(locz)

plt.savefig('trajectory.png')
