import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('ffbody.mov', delimiter=' ')
x = data[:,1]; y = data[:,2]; z = data[:,3]
t = np.arange(0, 1, 1e-3)

td = np.arange(0, 1, 0.1)
yd = np.zeros(len(td))
for i in range(0,len(td)):
	yd[i] = y[np.min(np.where(t>=td[i]))]

plt.plot(td, yd, marker='o', color='r')
plt.savefig('spaceTime.png')
