# Free Falling Body

+ MultiBody
+ Gravity
+ Discret Element
+ Space Time graph

 File                                   | Contents    
 :-------------                         | :-------------
 [ffbody.mbd](ffbody.mbd)               | MBDyn Input File
 [spaceTime.py](spaceTime.py)           | Python script to generate the space-time plot
 [Makefile](Makefile)                   | Makefile for running the analysis

## Model Description

First problem from the ADAMS tutorial kit.
Find the distance traveled by the mass after 1 second.

![](refs/ball.png)

Parameters   | Value
:----------  | :-------------
m            | 1 kg
V0           | 0 m/s
g            | 9.81 m/s^2   

## Reference Solution

The reference solution is obtained analytically:
```math
    s = s_0 + v_0 t + \frac{1}{2} g t^2 
```
```math
    s = 4.9 \text{m}
```

## How to run it

To run the analysis and generate the space-time graph
```
$ make all
```

The solver generetaes the following files:

 File                   | Contents    
 :-------------         | :-------------
 ffbody.mov             | File with the position of the node in each calculated step
 spaceTime.png          | Plot of position against time

## Solution obtained

The last line of the ffbody.mov shows the following position of the node
```
       1 0.000000e+00 -4.905000e+00 0.000000e+00 -0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 -9.810000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
```

The space time plot obtained is:
![](refs/spaceTime.png)

## Notes
