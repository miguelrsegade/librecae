# Cavity

+ Incompressible flow
+ IcoFoam

 File                                   | Contents    
 :-------------                         | :-------------
 [0/p](0/p)                             | Pressure initial and boundary conditions
 [0/U](0/U)                             | Velocity initial and boundary conditions
 [constant/transportProperties](constant/transportProperties) | Dictionary with the problem constants
 [system/controlDict](system/ControlDict) | General instructions on how to run the case
 [system/blockMeshDict](system/blockMeshDict) | blockMesh dictionary
 [system/sampleDict](system/sampleDict) | Dictionary with the sample locations and the fields to sample
 [system/decomposeParDict](system/decomposeParDict) | Mesh partitioning
 [system/fvSchemes](system/fvSchemes)    | Instructions for the discretization schemes
 [system/fvSolutions](system/fvSolutions) | Instructions on how to solve each discretized linear system

## Model Description

Example problem from [WolfDynamics](http://www.wolfdynamics.com/)

![](refs/cavity.png)


## Reference Solution

The reference solution is obtained from [this paper](docs/ghia.pdf).


