# CAD

* [FreeCAD](https://www.freecadweb.org/): 3D parametric modeling.
* [QCAD](https://qcad.org/): 2D CAD program.
* [LibreCAD](http://librecad.org): 2D CAD program, fork of QCAD.
* [BRL-CAD](http://brlcad.org/): 3D CSG and B-rep modeling.
* [OpenSCAD](http://www.openscad.org/): 3D solid modeling through programming.
    - [solidpython](https://github.com/SolidCode/SolidPython): A python frontend for modeling with OpenSCAD.
* [Blender](https://www.blender.org/): 3D graphic modeling suite.
* [meshlab](http://www.meshlab.net/): 3D triangular mesh modeling.
* [SolveSpace](http://solvespace.com/): 3D parametric modeling.
* [OpenVSP](http://www.openvsp.org/): Parametric modeling for flying vehicles.

# Visualización

* [Paraview](http://www.openvsp.org/): Data analysis and visualization application.
* [Visit](https://wci.llnl.gov/simulation/computer-codes/visit/): Scientific visualization and analysis software.
* [3dSlicer](https://www.slicer.org/): 3D visualization and image processing.
* [Mayavi](http://docs.enthought.com/mayavi/mayavi/): Scientific data visualization.

# Mallado

* [GMSH](http://gmsh.info/): Mesh generator, pre- and post-processor.
    - [pygmsh](https://github.com/nschloe/pygmsh): Python interface for gmsh.
* [Salome](http://www.salome-platform.org/): Generic platform for pre- and post-processing.
* [Netgen](https://ngsolve.org/): Automatic generation of tetrahedrical meshes.
* [Tetgen](http://wias-berlin.de/software/index.jsp?id=TetGen&lang=1): Triangular and tetrahedrical mesh generation.
* [MFEM](http://mfem.org/): Finite element discretization library.
* [enGRID](https://github.com/enGits/engrid): Mesh generation for CFD.
* [LaGrit](http://lagrit.lanl.gov/): Mesh generation by LANL
* [cfMesh](https://sourceforge.net/projects/cfmesh/): Library for volume mesh generation

# Solver

## Structural
* [Code Aster](https://www.code-aster.org/): Structural analysis and thermomechanical software.
* [Calculix](http://www.dhondt.de/): Finite elment structural software.
* [Warp3D](http://www.warp3d.net/): 3D Nonlinear analyisis of solids

## CFD
* [OpenFOAM](https://openfoam.org/): CFD software.
* [Code Saturn](https://www.code-saturne.org/): Fluid problems solver.
* [SU2](https://su2code.github.io/): CFD program suite
* [Nek5000](https://nek5000.mcs.anl.gov/): A Fast and scalable CFD solver
## Explicit
* [Impact](www.impact-fem.org/): Finite element suite for impact problems.
## Multibody
* [MBDyn](https://www.mbdyn.org/): Multi-body analysis software.
## Particles
* [YADE](https://yade-dem.org/doc/): Discret element method framework.
## General
* [Elmer](https://yade-dem.org/doc/): Multiphysics analysis software.
* [Agros2D](http://www.agros2d.org/): Numerical solution of physical fields application.
* [FreFem++](http://www.freefem.org/): Lenguaje para la resolución de ecuaciones diferenciales.
* [dealii](http://www.dealii.org/): Finite elements library.
* [fenics](http://www.dealii.org/): Computing platform for the resolution of differential equations.
* [kratos](http://www.cimne.com/kratos/): Multidisciplinary framework for simulations.
* [GetDP](http://getdp.info/): General enviroment for treatment of discrete problems.
* [FireDrake](http://www.firedrakeproject.org/): Automatic system for solving differential equations.
* [GetFem++](http://getfem.org/): Finite element library
* [libMesh](http://libmesh.github.io/): Numerical simulation framework
* [Oomph-lib](http://oomph-lib.maths.man.ac.uk/doc/html/index.html): Object oriented finite element library
* [NGsolve](https://ngsolve.org/): Finite element software for multiphysics problems
* [MoFEM](http://mofem.eng.gla.ac.uk/mofem/html/): Mesh oriented finite element method.
* [Albany](http://gahansen.github.io/Albany/): Implicit, unstructured grid, finite element code.
* [JuliaFEM](http://juliafem.org/): Software for reliable, scalable, distributed FEM.

# Solver assistant

* [Salome-Meca](https://www.code-aster.org/spip.php?article303): Salome version with Code Aster support.
* Calculix-Launcher: Interfaz gráfica para lanzar Calculix.
* HelyxOS: Interfaz gráfica para OpenFOAM.

# Análisis de resultados

* Dakota: Interfaz flexible entre códigos de análisis y métodos iterativos.
* OpenMDAO: Framework multidisciplinar para optimización y análisis.
* OpenTurns: Framework para el procesado de incertidumbre.

# Librería de geometría

* OpenCascade: Plataforma de desarrollo para CAD, CAM y CAE.
* CGAL: Librería de algoritmos  geometrícos.

# Librerías matemáticas

* Blas/Lapack: Paquete de álgebra lineal.
* Atlas: Herramientas para la compilación optimazada de Blas/Lapack.
* OpenBlas Implementación optimizada de Blas/Lapack.
* ACML Implementación optimizada de Blas/Lapack para procesadores AMD.
* Magma Versión de Blas/Lapack para sistemas heterogéneos.
* uBLAS Versión basada en plantillas C++ de Blas/Lapack.
* Blacs/Scalapack: Paquete de álgebra lineal en paralelo.
* Arpack: Subrutinas de Fortran para resolver problemas de autovalores.
* ArrayFire: Librería de propósito general para GPUs.
* Scotch: Paquete para particionado secuencial y paralelo de grafos.
* METIS: Particionado de grafos secuencial.
* ParMETIS: Particionado de grafos paralelo.
* Spooles: Solver de sistemas de ecuaciones lineales dispersas.
* GNU Scientific Library: Librería numérica para C y C++.
* GLPK Paquete para resolver problemas de programación lineal de gran escala.
* SuiteSparse: Suite de software para matrices dispersas.
* Armadillo: Librería de C++ para álgebra lineal.
* Eigen: Librería basada en plantillas para álgebra lineal.
* ALGLIB: Librería de análisis numérico y procesamiento de datos.
* MUMPS: Solver directo  disperso y paralelo.
* GMP: Librería para aritmética de precisión arbitraria.
* p4est: Refinado de mallas en colecciones de Octrees.
* Petsc: Suite de estructuras de datos y rutinas para resolver PDEs.
* Trilinos: Framework orientado a objetos para resolver PDEs.
* Sundials: Suite de solvers no lineales algebraicos y diferenciales.

# Python libraries
* [Numpy](http://www.numpy.org/): Fundamental package for numerical computations in Python.
* [Scipy](https://www.scipy.org/scipylib/): Efficient numerical routines in Python.
* [Matplotlib](https://matplotlib.org/): 2D Python plotting library.
* [Pandas](https://pandas.pydata.org/): Data structures and data analysis tools library.

# Utilities libraries

* glibc: Librería de C del proyecto GNU.
* Boost: Librerías de C++ revisadas por pares.
* Muparser: Parseador de expresiones matemáticas.
* TBB: Librería de plantillas de C++ para programación paralela.
* openmpi: Librería de paso de mensajes de alto rendimiento.
* openmp: Librería de tiempo de ejecución de OpenMP.
* zlib: Librería para comprimir datos.

# Lenguajes interpretados

* python: lenguaje de propósito general.
* octave: lenguaje para computación científica.
* bash: unix shell y lenguaje de comandos.
* scilab: software para computación numérica.
* maxima: sistema de álgebra computacional.
* sage: sistema de software matemático.
* R: lenguaje y entorno para estadística y gráficas.

# Librerías de datos y visualización

* hdf5: Modelo de datos, librería y formato de archivos para gestionar datos.
* vtk: Sistma para gráficos 3D, procesado de imágenes y visualización.
* netcdf: Suite para la creación y acceso de datos científicos.
* med: Estándar y modelo de datos para almacenar datos científicos.
* coin3d: Implementación libre de la API de Open Inventor.
* Qt: Framework para el desarrollo de interfaces gráficas.
* GTK: Framework para la creación de interfaces gráficas.
* CGNS: Sistema de notación general para CFD.
* FLTK: Toolkit ligero de C++ para desarrollo de interaces gráficas.

# Compiladores y herramientas de desarrollo

* [gcc](https://gcc.gnu.org/): GNU Compiler collection.
* [LLVM/clang](https://llvm.org/) Colección de herramientas de compilación.
* [make](https://www.gnu.org/software/make/): Tool for managing dependencies and build automation.
* [cmake](https://cmake.org/): Multiplatform tool designed to build, test and package software.
* [git](https://git-scm.com/): Version control software.
* [openmpi](https://www.open-mpi.org/): Message Passing Interface (MPI) implementation.
* [openmp](http://www.openmp.org/): API specification for parallel programming.
* [Linux](https://www.kernel.org/): Operating system kernel similar to Unix.

## Typesetting 
* Latex
* Tikz
* PSTricks

# Otros

* [ProjectChrono](https://projectchrono.org/): Multi-physics simulation engine.
* [xflr5](http://www.xflr5.com/): Tool for the analysis of airfoils and wings.
* [flightgear](http://home.flightgear.org/): Flight simulator.
* [MFront](http://tfel.sourceforge.net/): Code generation tool dedicated to material knowledge.
* [Precice](http://www.precice.org/): Coupling library for multiphysics simulations.
* [CMB](http://www.computationalmodelbuilder.org/): Application framework to be easily adapted to specific problem domains.
* [Xfoil](http://web.mit.edu/drela/Public/web/xfoil/): Interactive program for the design and analysis of subsonic isolated airfoils. 
* [OpenSees](http://opensees.berkeley.edu/): Open System for Earthquake Engineering Simulation.
* [Dedalus](http://dedalus-project.org/): A flexible framework for spectrally solving differential equations.

# License not clear

* Z88
* CFDEM
* Cast3M

# Not directly related

* [Cantera](http://www.cantera.org/docs/sphinx/html/index.html): Chemical kinetics, thermodynamics and transport tool suite
* [LAMMPS](http://lammps.sandia.gov/): Molecular dynamics software
* [Avogadro](https://avogadro.cc/): Advanced molecular editor
* [Atomsk](http://atomsk.univ-lille1.fr/): Program to the creation, manipulation and conversion of atomic systems.
* [OpenSim](https://simtk.org/projects/opensim): Extensible software for modeling, simulating, controlling and analyzing the neuromusculoskeletal system.
