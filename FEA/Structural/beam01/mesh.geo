elementSize = 5;

L = 3;

Point(1) = {0, 0, 0, elementSize};
Point(2) = {L, 0, 0, elementSize};
Point(3) = {2*L, 0, 0, elementSize};
Point(4) = {2*L, -0.5, 0, 1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};

// Number of elements
Transfinite Line {1,2} = 5;
Transfinite Line {3} = 1;

// GMSH can't create group of nodes 
// This will not create The group of Nodes needed in CA
//    Physical Point("Free") = {1};
//    Physical Point("Roller") = {2};
//    Physical Point("Fix") = {3};
// It will create groups of 0D elements 
// The groups of nodes are created using
// CA DEFI_GROUP command either using the 0D elemnets 
// or the node numbers

Physical Point("Fix") = {1};
Physical Point("Roller") = {2};
Physical Point("SpringUp") = {3};
Physical Point("SpringFix") = {4};

Physical Line("Beams") = {1, 2};
Physical Line("Spring") = {3};
