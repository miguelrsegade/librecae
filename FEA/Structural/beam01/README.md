# Springs01

+ Static Analysis
+ Beam elements

 File                                   | Contents    
 :-------------                         | :-------------
 [mesh.mail](mesh.mail)                 | Mesh in code_aster format
 [analysis.export](analysis.export)     | Export file for astk
 [Makefile](Makefile)                   | Makefile for running the analysis

## Model Description

Dynamic analysis of an articulated bar subjecet to a varying torque in one end

![](refs/beam.png)

Parameters   | Value
:----------  | :-------------
E            | 2.1e11 N/m2
nu           | 0.3
I            | 2e-4 m4
L            | 3 m
P            | 100 kN

## Reference Solution

## How to run it

## Solution obtained

## Notes

