# Springs01

+ Dynamic Analysis
+ Beam elements

 File                                   | Contents    
 :-------------                         | :-------------
 [mesh.mail](mesh.mail)                 | Mesh in code_aster format
 [analysis.export](analysis.export)     | Export file for astk
 [Makefile](Makefile)                   | Makefile for running the analysis

## Model Description

Dynamic analysis of an articulated bar subjecet to a varying torque in one end

![](refs/articulatedBar.png)

Parameters   | Value
:----------  | :-------------
E            | 2.1e11 N/m2
nu           | 0.3
L            | 1.5 m
T            | 1 Nm (varying according to function)

![](refs/torqueFunction.png)

## Reference Solution

## How to run it

## Solution obtained

## Notes

