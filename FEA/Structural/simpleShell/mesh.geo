W = 60;
H = 25;

gridSize=10;

Point(1) = {0, 0, 0, gridSize};
Point(2) = {H, 0, 0, gridSize};
Point(3) = {H, W, 0, gridSize};
Point(4) = {0, W, 0, gridSize};


Line(5) = {1,2};
Line(6) = {2,3};
Line(7) = {3,4};
Line(8) = {4,1};

Line Loop(9) = {5,6,7,8};
Plane Surface(10) = 9;


Transfinite Line{5,7} = H/gridSize;
Transfinite Line{6,8} = W/gridSize;
Transfinite Surface{10};
Recombine Surface{10};

// Force the point to be in the mesh
// NOT WORKING WITH STRUCTURED MESH
Point(11) = {H/3, W/4, 0, gridSize};
Point{11} In Surface{10};
Physical Line("Fix") = {5};
Physical Point("Load") = {11};
Physical Surface("All") = {10};

Mesh.CharacteristicLengthMin = 0;
Mesh.CharacteristicLengthMax = 10;

Mesh 2;

Save 'mesh.med';
