settings.outformat="pdf";
settings.render=0;
settings.prc=false;
import three;
currentprojection = obliqueX;
size(8cm, 0);
draw(O -- 4X, L=Label("$x$", position=EndPoint));
draw(O -- 7Y, L=Label("$y$", position=EndPoint));
draw(O -- 2Z, L=Label("$z$", position=EndPoint));
draw(plane(O=O, 2.5X, 6Y), blue);
triple load = 1.25X+6Y;
draw(load--load-Z, arrow=Arrow3(emissive(black)));
label("P", load-0.5Z, E);
