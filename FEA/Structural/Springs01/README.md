# Springs01

+ Static Analysis
+ Discret Elements
+ Stiffness matrix

 File                                   | Contents    
 :-------------                         | :-------------
 [analysis.comm](analysis.comm)             | Code Aster command file
 [mesh.mail](mesh.mail)                 | Mesh in code_aster format
 [analysis.export](analysis.export)     | Export file for astk
 [Makefile](Makefile)                   | Makefile for running the analysis

## Model Description

Problem obtained from 
Static analysis of three springs connected in series and subjected to a force.

![](refs/springs01.png)

Parameters   | Value
:----------  | :-------------
k1           | 1000 lb/in
k2           | 2000 lb/in
k3           | 3000 lb/in
P            | 5000 lb

## Reference Solution

The reference solution is obtained analytically in D. Logan book:
```math
    u_3 = \frac{10}{11} \text{in} \qquad u_4 = \frac{15}{11} \text{in}
```

## How to run it

Beacause this is a very simple model, the geometry of the mesh is created directly 
in [mesh.mail](mesh.mail) using a text editor.
    
To launch the study:
```
$ make
```

The solver generetaes the following files:

 File                   | Contents    
 :-------------         | :-------------
 analysis.resu          | Result file displacements and tension of each spring
 stiffness.npy          | Numpy array containining global stiffness matrix

## Solution obtained

Values of the displacement at the nodes present in analysis.resu file:
```
 NOEUD                 DX                    DY      
 P1        0.00000000000000E+00  0.00000000000000E+00
 P2        0.00000000000000E+00  0.00000000000000E+00
 P3        9.09090909090909E-01  0.00000000000000E+00
 P4        1.36363636363636E+00  0.00000000000000E+00
```

## Notes

The following steps could be included in a single `MECA_STATIQUE` command:
+ Calculate the elementary vector and matrix 
+ Enumeration of the system of equations
+ Assembly of the vectors and matrix
+ Facotrize the matrix
+ Solve the system of equations
+ Create a concept of type resu, to be able to calculate derived fields

The model exports the stiffness matrix to stiffness.npy, which can be loaded in
numpy using:

```
np.load('stiffness.npy')
```

The matrix obtained using the direct stiffness method is:
```math
\begin{bmatrix}
1000 & 0  & -1000  & 0\\ 
0 & 3000  & 0 & -3000 \\ 
 -1000& 0 & 3000 & -2000 \\ 
0 & -3000  & -2000 & 5000
\end{bmatrix}
```

