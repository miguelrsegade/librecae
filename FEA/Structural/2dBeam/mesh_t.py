# -*- coding: utf-8 -*-

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import os
ExportPath=os.path.join(os.getcwd(),'')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

geompy = geomBuilder.New(theStudy)

def getSubEdgeIDsByCoords(Partition, coordx, coordy, coordz):
    vertex = geompy.MakeVertex(coordx, coordy, coordz)
    func = geompy.GetEdgeNearPoint
    shape = func(Partition, vertex)
    edgeID = geompy.GetSubShapeID(Partition, shape)
    return edgeID

def getSubVertexIDsByCoords(Partition, coordx, coordy, coordz):
    vertex = geompy.MakeVertex(coordx, coordy, coordz)
    func = geompy.GetVertexNearPoint
    shape = func(Partition, vertex)
    edgeID = geompy.GetSubShapeID(Partition, shape)
    return edgeID

L = {{L}}
H = {{H}}

marker = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
sk = geompy.Sketcher2D()
sk.addPoint(0.0, 0.0)
sk.addSegmentAbsolute(0.0, H)
sk.addSegmentAbsolute(L, H)
sk.addSegmentAbsolute(L, 0.0)
sk.close()
Sketch = sk.wire(marker)
Beam = geompy.MakeFaceWires([Sketch], 1)

# Groups
# Fix Groups
Fix = geompy.CreateGroup(Beam, geompy.ShapeType["VERTEX"])
fixVertexIds = [getSubVertexIDsByCoords(Beam, 0, H/2, 0)]
fixVertexIds.append(getSubVertexIDsByCoords(Beam, L, H/2, 0))
geompy.UnionIDs(Fix, fixVertexIds)


# Load Edge Groups
Load = geompy.CreateGroup(Beam, geompy.ShapeType["EDGE"])
loadIds = [getSubEdgeIDsByCoords(Beam, L/2, H, 0)]
geompy.UnionIDs(Load, loadIds)

geompy.addToStudy( Beam, 'Beam' )
geompy.addToStudyInFather( Beam, Fix, 'Fix' )
geompy.addToStudyInFather( Beam, Load, 'Load' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

meshSize = {{meshSize}}

smesh = smeshBuilder.New(theStudy)
Mesh = smesh.Mesh(Beam)
Regular_1D = Mesh.Segment()
Local_Length = Regular_1D.LocalLength(meshSize, None,1e-07)
Quadrangle_2D = Mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
isDone = Mesh.Compute()
Fix = Mesh.GroupOnGeom(Fix,'Fix',SMESH.NODE)
Load = Mesh.GroupOnGeom(Load,'Load',SMESH.EDGE)

## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Local_Length, 'Local Length')
smesh.SetName(Mesh.GetMesh(), 'Mesh')

try:
  Mesh.ExportMED(os.path.join(ExportPath,'mesh.med'), 0, SMESH.MED_V2_2, 1, None ,1)
  pass
except:
  print 'ExportToMEDX() failed. Invalid file name?'

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
