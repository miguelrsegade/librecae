# -*- coding: utf-8 -*-

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import os
ExportPath=os.path.join(os.getcwd(),'')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Face = geompy.MakeFaceHW(1, 1, 1)
Boundary = geompy.CreateGroup(Face, geompy.ShapeType["EDGE"])
geompy.UnionIDs(Boundary, [3, 6, 8, 10])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Face, 'Face' )
geompy.addToStudyInFather( Face, Boundary, 'Boundary' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New(theStudy)
Mesh = smesh.Mesh(Face)
Regular_1D = Mesh.Segment()
Local_Length_1 = Regular_1D.LocalLength(0.2,None,1e-07)
Quadrangle_2D = Mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
isDone = Mesh.Compute()
Boundary_1 = Mesh.GroupOnGeom(Boundary,'Boundary',SMESH.EDGE)


## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Local_Length_1, 'Local Length_1')
smesh.SetName(Mesh.GetMesh(), 'Mesh')
smesh.SetName(Boundary_1, 'Boundary')

try:
  Mesh.ExportUNV(os.path.join(ExportPath,'mesh.unv'))
  pass
except:
  print 'ExportUNV() failed. Invalid file name?'


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(True)
