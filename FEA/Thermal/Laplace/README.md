# Laplace equation

*The following problem statement has been provided by Prof. Michel Louge, Cornell University
for the ENGR2000X Edx Online Course.*

Find the dimensionless temperature:
```math
\theta \equiv \frac{T-T_0}{T_0-T_\infty}
```

on a rectangular domain with conductivity $k$ subject to isothermal ($`T_0`$) bottom boundary,
adiabatic top and left and right boundary exposed to a fluid ($`T=T_\infty`$) with constant
convection coefficient $`h`$. 

Using dimensionless coordinates $`x^* \equiv x/W`$ and $`y^* \equiv y/W`$  the problem is shown 
in the following figure (dropping the $`*`$ for convinience:

![](refs/laplace.png)

## Parameters

Parameters   | Value
:----------  | :-------------
$`H^* = H/W`$  | 2
$`\mathrm{Bi} = hW/k`$  | 5


To solve this non-dimensional BVP numerically we need to  specify the dimensional values whose
results are equivalent, to do that we use:

Parameters   | Value
:----------  | :-------------
$`T_0`$        | 1ºC
$`T_\infty`$  | 0ºC
$`H`$         | 2 m
$`W`$         | 1 m
$`h`$         | $`\mathrm{Bi}=5`$
$`k`$         | 1 W/mºC


![](refs/dimension.png)

The results obtained will be in ºC, but can be interpreted as dimensionenless ($`\theta \equiv T`$).
