from paraview.simple import *
import os

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

cwd = os.getcwd()
resultFile=os.path.join(cwd, 'laplace0001.vtu')

# create a new 'XML Unstructured Grid Reader'
laplace0001vtu = XMLUnstructuredGridReader(FileName=[resultFile])

# get active view
renderView = GetActiveViewOrCreate('RenderView')
# Set a specific view size
renderView.ViewSize = [1122, 811]

# Hide Orientation axis 
renderView.OrientationAxesVisibility = 0
# show data in view
laplace0001vtuDisplay = Show(laplace0001vtu, renderView)
# trace defaults for the display properties.
laplace0001vtuDisplay.Representation = 'Surface With Edges'

# set scalar coloring
ColorBy(laplace0001vtuDisplay, ('POINTS', 'temperature'))


# get color transfer function/color map for 'temperature'
temperatureLUT = GetColorTransferFunction('temperature')
temperatureLUT.RGBPoints = [0.04545220627566756, 0.0, 0.0, 1.0, 1.0000000000000002, 1.0, 0.0, 0.0]
temperatureLUT.ColorSpace = 'HSV'
temperatureLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
temperatureLUT.NumberOfTableValues = 8
temperatureLUT.ScalarRangeInitialized = 1.0

# current camera placement for renderView1
renderView.InteractionMode = '2D'
renderView.CameraPosition = [0.5, 1.0, 4.0]
renderView.CameraFocalPoint = [0.5, 1.0, 0.0]

# get color legend/bar for temperatureLUT in view renderView1
temperatureLUTColorBar = GetScalarBar(temperatureLUT, renderView)

# change scalar bar placement
temperatureLUTColorBar.WindowLocation = 'AnyLocation'
temperatureLUTColorBar.Position = [0.7, 0.35]
temperatureLUTColorBar.ScalarBarLength = 0.3

# Properties modified on temperatureLUTColorBar
temperatureLUTColorBar.Title = 'Temperature'
temperatureLUTColorBar.ComponentTitle = ''

# save screenshot
SaveScreenshot(os.path.join(cwd,'temperature.png'), 
               renderView, ImageResolution=[1122, 811], TransparentBackground=1)
