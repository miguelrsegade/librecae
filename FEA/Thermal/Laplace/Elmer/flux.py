from paraview.simple import *
import os

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

cwd = os.getcwd()
resultFile=os.path.join(cwd, 'laplace0001.vtu')

# create a new 'XML Unstructured Grid Reader'
laplace0001vtu = XMLUnstructuredGridReader(FileName=[resultFile])

# get active view
renderView = GetActiveViewOrCreate('RenderView')
# Set a specific view size
renderView.ViewSize = [1122, 811]

# Hide Orientation axis 
renderView.OrientationAxesVisibility = 0
# show data in view
laplace0001vtuDisplay = Show(laplace0001vtu, renderView)
# trace defaults for the display properties.
laplace0001vtuDisplay.Representation = 'Surface With Edges'

# create a new 'Glyph'
glyph1 = Glyph(Input=laplace0001vtu, GlyphType='Arrow')
glyph1.ScaleFactor = 0.2
glyph1.GlyphTransform = 'Transform2'

# Properties modified on glyph1
glyph1.Vectors = ['POINTS', 'temperature flux']

# show data in view
glyph1Display = Show(glyph1, renderView)

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'

# set scalar coloring
ColorBy(glyph1Display, ('POINTS', 'temperature flux', 'Magnitude'))

# get color transfer function/color map for 'temperatureflux'
fluxLUT = GetColorTransferFunction('temperatureflux')

# current camera placement for renderView1
renderView.InteractionMode = '2D'
renderView.CameraPosition = [0.5, 1.0, 4.0]
renderView.CameraFocalPoint = [0.5, 1.0, 0.0]

# get color legend/bar for fluxLUT in view renderView1
fluxLUTColorBar = GetScalarBar(fluxLUT, renderView)

# change scalar bar placement
fluxLUTColorBar.WindowLocation = 'AnyLocation'
fluxLUTColorBar.Position = [0.8, 0.35]
fluxLUTColorBar.ScalarBarLength = 0.3

# Properties modified on fluxLUTColorBar
fluxLUTColorBar.Title = 'Temperature Flux'
fluxLUTColorBar.ComponentTitle = ''

# save screenshot
SaveScreenshot(os.path.join(cwd,'flux.png'), 
               renderView, ImageResolution=[1122, 811], TransparentBackground=1)
