#### import the simple module from the paraview
from paraview.simple import *
import os 

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

cwd = os.getcwd()
resultFile=os.path.join(cwd, 'laplace0001.vtu')

# create a new 'XML Unstructured Grid Reader'
laplace0001vtu = XMLUnstructuredGridReader(FileName=[resultFile])

# get active view
renderView = GetActiveViewOrCreate('RenderView')
# Set a specific view size
renderView.ViewSize = [1122, 811]

# create a new 'Plot Over Line'
plotOverLine1 = PlotOverLine(Input=laplace0001vtu,
            Source='High Resolution Line Source')

# Properties modified on plotOverLine1.Source
plotOverLine1.Source.Point1 = [0.0, 1.0, 0.0]
plotOverLine1.Source.Point2 = [1.0, 1.0, 0.0]

# Create a new 'Line Chart View'
lineChartView1 = CreateView('XYChartView')
lineChartView1.ViewSize = [591, 811]
lineChartView1.LeftAxisRangeMaximum = 6.66
lineChartView1.BottomAxisRangeMaximum = 6.66
lineChartView1.RightAxisRangeMaximum = 6.66
lineChartView1.TopAxisRangeMaximum = 6.66

# get layout
layout1 = GetLayout()

# place view in the layout
layout1.AssignView(2, lineChartView1)

# show data in view
plotOverLine1Display_1 = Show(plotOverLine1, lineChartView1)
# trace defaults for the display properties.
plotOverLine1Display_1.SeriesVisibility = ['temperature']


# update the view to ensure updated data information
lineChartView1.Update()

# export view
ExportView(os.path.join(cwd,'plotOverLine.ps'), view=lineChartView1)

