from pint import UnitRegistry
import csv

# Read the area result
group = 'holeDown'
with open('areas.txt') as fd:
    reader=csv.reader(fd, delimiter=' ')
    area=[float(row[1]) for row in reader if row[0] == group]

ureg = UnitRegistry(system='mks')

area = area[0] * ureg.m**2
F = 190 * ureg.lbf
w = 60 * ureg.rpm
d = 25 * ureg.mm
fric = 0.14
fForce = fric*F
fMoment = fForce*d/2
fPower = fMoment*w
transfer = 0.5*fPower/area

print(transfer.to_base_units().magnitude)
