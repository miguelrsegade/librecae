// Scale the geometry to meters
Geometry.OCCScaling = 0.001;

Merge '6254K240.step';


// Mesh parameters
Mesh.CharacteristicLengthMin = 0;
Mesh.CharacteristicLengthMax = 0.0025;
// Algorithms
Mesh.Algorithm = 6; // 2D Frontal
Mesh.Algorithm3D = 4; // 3D Frontal

// As we define a group of surfaces we need to define a group with all the surfaces
// so the export is correct
Physical Surface("holeDown") = {3};

Physical Surface("Exterior") = {54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 4};
Physical Surface("Exterior") += {8, 17, 9, 16, 10, 18, 11, 19, 12, 15, 13, 20, 6};
Physical Surface("Exterior") += {41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 5};

Physical Volume("volumeGroup") = {1};

Physical Surface("surfaceGroup") = {4, 49, 48, 47, 11, 19, 18, 5, 34, 35, 36, 43, 42, 44};
Physical Surface("surfaceGroup") +={46, 45, 22, 21, 2, 17, 50, 24, 30, 8, 51, 23, 10, 29};
Physical Surface("surfaceGroup") +={9, 16, 31, 33, 32, 52, 14, 53, 54, 3, 12, 26, 25, 1};
Physical Surface("surfaceGroup") +={37, 15, 38, 28, 7, 27, 13, 20, 39, 40, 6, 41};

// Save group of nodes 
Mesh.SaveGroupsOfNodes = 1;

Mesh 3;

Save '6254K240.msh';
